# mitprojekt



## Uvod

Prostě <strong>topova</strong> věc aby sis neurval prsty při zapalování ohňostroje, protože mamka vždy říkává: Žádný takový, odpalovat jen pod dozorem tatínka.

Verze odpalovacího zařízení je pojmenováno podle prvního brnění Tonyho Starka (Iron mana) <strong>MARK01</strong>.

## Projekt je složen z 3 hlavních částí:

- OVLADAČ                    (start)

- DÁLKOVÉ RELÉ               (middle)       +programovatelná deska

- odpalované zařízení        (end)          //třetí část bohužel
                                            nebyla uplatněna z
                                            důvodu možnosti 
                                            označení za teroristu.


![schéma](Pictures/schema.PNG)


# OVLADAČ

K ovladači byly použity tyto součástky:
- [Univerzální dálkové ovládání KL-812 na 12V](https://www.zabezpecovaci-zarizeni.cz/bezdratove-ovladani/kl-moduly/univerzalni-dalkove-ovladani-kl-812-na-12v-%5BCIP032%5D) - pouze ovladač
- tlačítka , ledky 

![FOTKA](Pictures/20220621_212000.jpg)

# DÁLKOVÉ RELÉ + programovatelná deska

k střední části byly použity tyto součástky:

- [Univerzální dálkové ovládání KL-812 na 12V](https://www.zabezpecovaci-zarizeni.cz/bezdratove-ovladani/kl-moduly/univerzalni-dalkove-ovladani-kl-812-na-12v-%5BCIP032%5D)
- [Arduino UNO R3](https://dratek.cz/arduino/1258-eses-klon-arduino-uno-r3-ch340.html?gclid=Cj0KCQjwkruVBhCHARIsACVIiOz60yIbqhC74ylRFwgSM55V0poN24QlZOyQzeAf4E5P09wFzWkJLKcaAgeuEALw_wcB)
- [Nastavitelny měnič DC-DC](https://dratek.cz/arduino/1303-stepdown-nastavitelny-menic-s-lm2596-dc-dc.html?gclid=Cj0KCQjwkruVBhCHARIsACVIiOxoEpBXKSKqkKQN13xLw4oZBuvKth6YjD6rQXYGhwHVCibnCEa71YQaAh_1EALw_wcB)
- [LCD Display 16x2](https://dratek.cz/arduino/836-display-modry-16x2-znaku.html?gclid=Cj0KCQjwkruVBhCHARIsACVIiOzp7cVgbdzBWF4x3VyC_mIjDsez-2d9Akbi8dpHY3-kvn8RZQC4T0EaArYAEALw_wcB)
- Battery
- [Buzzer](https://dratek.cz/arduino/2063-signalni-led-svetlo-a-bzucak-ac-dc-12v-cerveny.html)

## Schéma střední části:

![Schéma](Pictures/rele1.PNG)

![FOTKA2](Pictures/20220621_212925.jpg)


